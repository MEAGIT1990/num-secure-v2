import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConnexionComponent } from './components/authentication/connexion/connexion.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { ForgetPasswordComponent } from './components/authentication/forget-password/forget-password.component';
import { EditPasswordComponent } from './components/authentication/edit-password/edit-password.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { HttpClientModule } from '@angular/common/http';
import { FormBuilder, FormsModule } from '@angular/forms';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { AdminComponent } from './components/admin/admin.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgBusyModule } from 'ng-busy';
import { DatePipe } from './core/pipes/date.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { OtpModalComponent } from './components/authentication/connexion/otp-modal/otp-modal.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { SharedModule } from './shared/shared.module';
import { AuthAdminComponent } from './components/authentication/auth-admin/auth-admin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';


@NgModule({
    declarations: [
        AppComponent,
        ConnexionComponent,
        AccueilComponent,
        ForgetPasswordComponent,
        EditPasswordComponent,
        PageNotFoundComponent,
        MainLayoutComponent,
        AdminComponent,
        DatePipe,
        OtpModalComponent,
        AuthAdminComponent,
        DashboardComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        SharedModule

    ],
    providers: [BsModalService, FormBuilder],
    bootstrap: [AppComponent]
})
export class AppModule { }

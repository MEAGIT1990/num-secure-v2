import { Component } from '@angular/core';
import { AutoLogoutService } from './core/services/auto-logout.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Gestion des numeros de sécurité sociale';
  constructor(private autotLogout: AutoLogoutService){

  }
}

import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { AuthService } from "../services/auth.service";


@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log("this.authService.getUser(): ",this.authService.getUser());
    
    if (!this.estVide(this.authService.getUser())) {
      return true;
    }
    
    this.authService.logout()
    return false;
  }

  estVide(objet: { hasOwnProperty: (arg0: string) => any; }) {
    for (var prop in objet) {
      if (objet.hasOwnProperty(prop))
        return false;
    }
    return true;
  }
}

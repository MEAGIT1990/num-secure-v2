import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RestClientService {
  baseUrl = environment.baseUrl
  constructor(private http: HttpClient) { }
  execute(endPoint: any, data: any): Observable<any> {
    return this.http.post(this.baseUrl + '/' + endPoint, data)
  }

  publicApi(url: any): Observable<any> {
    return this.http.get(url)
  }
}


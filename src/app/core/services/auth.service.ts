import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { OtpModalComponent } from 'src/app/components/authentication/connexion/otp-modal/otp-modal.component';
import { RestClientService } from './rest-client.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    modalRef: any
    constructor( private toastr: ToastrService,private router: Router, private restClient: RestClientService, private modalService: BsModalService) { }

    accueil() {
        this.router.navigate(['/accueil'])
    }

    logout() {
        localStorage.removeItem("currentUser");
        this.router.navigate(['/login'])
    }
    getUser() {
        return JSON.parse(localStorage.getItem("currentUser") || '{}');
    }
    getLoginInfo() {
        return JSON.parse(localStorage.getItem("loginInfo") || '{}');
    }

    admin() {
        this.router.navigate(['/admin'])
    }

    login(user: any) {
        let request = {
            data: {
                login: user.login,
                // email: user.email,
                password: user.password
            }
        }
        let promise =  new Promise((resolve, reject) => {
            this.restClient.execute('admin/users/auth', request).subscribe(
                (res: any) => {
                    if (!res.hasError) {
                        localStorage.setItem("currentUser", JSON.stringify(res.items[0]));
                        res.items[0].isdsi ? this.router.navigate(['/admin']) : this.router.navigate(['/accueil'])
                        resolve(false)

                    }
                    else {
                        this.toastr.error(res.status.message, 'Erreur!');
                        resolve(false)

                    }
                },
                (error: any) => {
                    this.toastr.error('Erreur de communication', 'Erreur!');
                    resolve(false)

                }
            )
        })
        return promise
        
    }
    loginAdminAuth(user: any) {
        let request = {
            data: {
                login: user.login,
                // email: user.email,
                password: user.password
            }
        }
        let promise =  new Promise((resolve, reject) => {
            this.restClient.execute('benefCmu/adminAuth', request).subscribe(
                (res: any) => {
                    if (!res.hasError) {
                        localStorage.setItem("currentUser", JSON.stringify(res.items[0]));
                        this.router.navigate(['/dashboard'])
                        resolve(false)

                    }
                    else {
                        this.toastr.error(res.status.message, 'Erreur!');
                        resolve(false)

                    }
                },
                (error: any) => {
                    this.toastr.error('Erreur de communication', 'Erreur!');
                    resolve(false)

                }
            )
        })
        return promise
        
    }

    // login(user: any) {
    //     let request = {
    //         data: {
    //             email: user.email,
    //             mot_de_passe: user.password
    //         }
    //     }

    //     this.restClient.execute('iwUsers/login', request).subscribe(
    //         (res: any) => {
    //             if (!res.hasError) {
    //                 localStorage.setItem("currentUser", JSON.stringify(res.items[0]));
    //                 this.router.navigate(['/accueil'])
    //             }
    //             else {
    //                 alert(res.status.message)
    //             }
    //         },
    //         (error: any) => {
    //             alert('Problème de connexion')
    //         }
    //     )
    // }
    
    loginAssure(assure: any) {
        let request = {
            data: {
                matric_benef: assure.matric_benef,
                tel_benef: assure.tel_benef,
                dt_naiss_benef: assure.dt_naiss_benef,
                otp: assure.otp
            }
        }
        let promise = new Promise((resolve, reject) => {
            this.restClient.execute('benefCmu/auth', request).subscribe(
                (res: any) => {
                    console.log('res: ',res);
                    
                    if (!res.hasError) {
                        if (res.isFirstAttempt) {
                            // alert(res.status.message)
                            //ouvrir le modal ici
                            localStorage.setItem("loginInfo", JSON.stringify(request));
                            this.openModal(assure)
                            resolve(false)
                        }
                        else {
                            // this.modalRef.hide()
                            localStorage.setItem("loginInfo", JSON.stringify(res.items[0]));
                            // this.router.navigate(['/accueil'])
                            resolve(false)

                        }
                    }
                    else {
                        this.toastr.error(res.status.message, 'Erreur!');
                        resolve(false)

                    }
                },
                (error: any) => {
                    this.toastr.error('Erreur de communication', 'Erreur!');
                    resolve(false)

                }
            )
        });

        return promise

       
    }

    

    openModal(usr: any) {
        let config = { backdrop: true, ignoreBackdropClick: true, class: 'modal-custom-otp' };
        this.modalRef = this.modalService.show(OtpModalComponent, config);
        this.modalRef.content.currentData = usr;
    }



}

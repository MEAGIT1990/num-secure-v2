import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './components/accueil/accueil.component';
import { AdminComponent } from './components/admin/admin.component';
import { AuthAdminComponent } from './components/authentication/auth-admin/auth-admin.component';
import { ConnexionComponent } from './components/authentication/connexion/connexion.component';
import { EditPasswordComponent } from './components/authentication/edit-password/edit-password.component';
import { ForgetPasswordComponent } from './components/authentication/forget-password/forget-password.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AuthGuard } from './core/guards/auth.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';

const routes: Routes = [

  {path:'',redirectTo:'login', pathMatch: 'full' },               
  
  // {
  //   path: 'login', component: ConnexionComponent
  // },
  {
    path: 'login', component: ConnexionComponent
  },
  {
    path: 'admin', component: AuthAdminComponent
  },
  {
    path: 'dashboard', component: DashboardComponent
  },
  // {path:'authentification', component:AuthAdminComponent },
  // {
  //   path: 'reset_password', component: ForgetPasswordComponent
  // },
  // {
  //   path:'edit_password', component: EditPasswordComponent
  // },
  {
    path: '', component: MainLayoutComponent,
    children: [
      {
        path: 'accueil', component: AccueilComponent,canActivate: [AuthGuard],
      },
      // {
      //   path: 'admin', component: AdminComponent
      // },
    ]
  },
  { path: "**", component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true, relativeLinkResolution: "legacy" })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

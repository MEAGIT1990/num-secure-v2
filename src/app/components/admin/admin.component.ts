import { Component, OnInit, TemplateRef } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { RestClientService } from 'src/app/core/services/rest-client.service';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  society: string = 'MCI CARE COTE D\'IVOIRE'
  listAgent: any = []
  listPolice: any = []
  listAssureur: any = []
  index = 1;
  tableSize: number = 7;
  size = 20
  user: any
  modalRef: any;
  itemToSave: any = {}
  totalItems = 64;
  currentPage = 4;
  busyCall: Subscription | undefined;
  lbc_assure: any;
  prechecked: any=[];
  currentSelecteGarant: any={};
  constructor(private authService: AuthService, private restClient: RestClientService, private modalService: BsModalService, private toastr: ToastrService, private router: Router) {
    this.user = this.authService.getUser()
  }


  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }
  openModal(template: TemplateRef<any>, data?: any) {
    let config = { backdrop: true, ignoreBackdropClick: true, class: 'modal-custom' };
    this.itemToSave = {}
    this.listPolice=[]
    if (data) {
        this.itemToSave = { ...data }
        this.prechecked = this.itemToSave.polices
        console.log('list precheked: ',this.prechecked);
        
      }
    if (!this.user.isdsi) {
        this.prechecked=[]
      this.getPoliceByGarant()
    }
    
    this.modalRef = this.modalService.show(template, config);

  }

  confirmSave(data: any) {

    let checkedPolices = []

    checkedPolices = this.listPolice.filter((cp: any) => cp.isChecked)
    if (!data || !data.nom) {
      this.toastr.error('Veuillez renseigner nom !', 'Erreur!');
      return
    }
    if (!data || !data.prenom) {
      this.toastr.error('Veuillez renseigner prenom !', 'Erreur!');
      return
    }
    if (!data || !data.login) {
      this.toastr.error('Veuillez renseigner login !', 'Erreur!');
      return
    }
    if (!data || !data.contact) {
      this.toastr.error('Veuillez renseigner contact !', 'Erreur!');
      return
    }
    if (!checkedPolices || !checkedPolices.length) {
      this.toastr.error('Veuillez choisir au moins une police !', 'Erreur!');
      return
    }

    data.datasPolice = []
    checkedPolices.map((cp: any) => {
      data.datasPolice.push({ num_interne_pol: cp.num_interne_pol })
    })
    console.log('data to save: ', data);

    Swal.fire({
      title:'Attention',
      text:'Voulez vous sauvegarder cette donnée?',
      icon:'warning',
      showCancelButton: true,
      confirmButtonText:'Oui',
      confirmButtonColor: '#426DA9',
      cancelButtonColor: 'red',
      cancelButtonText:'Non'
    }).then((result) => {
      if (result.value){
        this.saveItem(data)
      } else {
        this.router.navigate(['/admin'])
      }
    })

    // let result = confirm("Voulez vous sauvegarder ces données?");
    // if (result == true) {
    //   this.saveItem(data)
    // } else {

    // }
  }

  confirmDelete(data: any) {
    Swal.fire({
      title:'Attention',
      text:'Voulez vous supprimer cette donnée?',
      icon:'warning',
      showCancelButton: true,
      confirmButtonText:'Oui',
      confirmButtonColor: '#426DA9',
      cancelButtonColor: 'red',
      cancelButtonText:'Non'
    }).then((result) => {
      if (result.value){
        this.deleteAdmin(data)
      } else {
        this.router.navigate(['/admin'])
      }
    })
    
    // let result = confirm("Voulez vous supprimer cette donnée?");
    // if (result == true) {
    //   this.deleteAdmin(data)
    // } else {

    // }
  }
  onTableDataChange(event: any) {
    this.index = event;
    this.getAdmin();
  }

  onSelectGarant(currentSelectedGarant: any) {
    this.currentSelecteGarant = currentSelectedGarant
    this.getPoliceByGarant(currentSelectedGarant)

  }


  getPoliceByGarant(currentSelectedGarant?: any) {
    console.log('this.itemToSave.lbc_assur: ', this.itemToSave.lbc_assur);

    let request = {
      index: this.index,
      size: this.size,
      user: this.user.id,
      data: {
        lbc_assur: currentSelectedGarant?.lbc_assur || this.user?.assureur
      }
    }

    console.log('test')

    this.busyCall = this.restClient.execute('police/getByCriteria', request).subscribe(
      (res: any) => {
        if (!res.hasError) {

          // this.listAgent = res.items
          // console.log('list agent: ', this.listAgent);
          // this.totalItems = res['count']
          this.listPolice = res.items
          this.listPolice.map((lp:any)=>lp.isChecked=false)
          if(this.prechecked && this.prechecked.length){
            this.listPolice.map((lp:any)=>{
                lp.isChecked= !!(this.prechecked.filter((pc:any) => pc.num_interne_pol == lp.num_interne_pol).length)
            })
          }
          console.log('listPolice tf: ',this.listPolice);
          
        //   this.getAdmin()

        }
        else {
          // Swal.fire({
          //   title:'Attention',
          //   text: res.status.message,
          //   icon:'warning'
          // })
          this.toastr.error(res.status.message, 'Erreur!');

          // alert(res.status.message)
        }

      },
      (err: any) => {
        
        this.toastr.error('Erreur de connexion', 'Erreur!');
        // Swal.fire({
        //   title:'Erreur',
        //   text:'erreur de connexion',
        //   icon:'error'
        // })
        // alert('erreur de connexion')
      },

    )
  }



  deleteAdmin(item: any) {
    let request = {
      user: this.user.id,
      datas: [
        item
      ]
    }

    this.busyCall = this.restClient.execute('iwCmuUser/delete', request).subscribe(
      (res: any) => {
        if (!res.hasError) {
          // this.listAgent = res.items
          // console.log('list agent: ', this.listAgent);
          // this.totalItems = res['count']
          this.getAdmin()

        }
        else {

          this.toastr.error(res.status.message, 'Erreur!');
          // Swal.fire({
          //   title:'Attention',
          //   text: res.status.message,
          //   icon:'warning'
          // })
          // alert(res.status.message)
        }

      },
      (err: any) => {

        this.toastr.error('Erreur de connexion', 'Erreur!');
        // Swal.fire({
        //   title:'Erreur',
        //   text:'erreur de connexion',
        //   icon:'error'
        // })
        // alert('erreur de connexion')
      },

    )

  }

  

  getAdmin() {
    let request = {
      index: this.index,
      size: this.size,
      user: this.user.id,
      data: {
        assureur: this.user.isdsi ? null : this.user.assureur
      }
    }

    this.busyCall = this.restClient.execute('iwCmuUser/getByCriteria', request).subscribe(
      (res: any) => {
        if (!res.hasError) {
          this.listAgent = res.items
          console.log('list agent: ', this.listAgent);
          this.totalItems = res['count']

        }
        else {
          this.toastr.error(res.status.message, 'Erreur!');
          // Swal.fire({
          //   title:'Erreur',
          //   text: res.status.message,
          //   icon:'warning'
          // })
          // alert(res.status.message)
        }

      },
      (err: any) => {

        this.toastr.error('Erreur de connexion', 'Erreur!');
        // Swal.fire({
        //   title:'Erreur',
        //   text:'erreur de connexion',
        //   icon:'error'
        // })
        // alert('erreur de connexion')
      },

    )
  }

  saveItem(item: any) {
    item.assureur = this.currentSelecteGarant.lbc_assure || this.user?.assureur
    let request = {
      user: this.user.id,
      datas: [
        item

      ]
    }

    this.busyCall = this.restClient.execute('iwCmuUser/' + (item.id ? 'update' : 'create'), request).subscribe(
      (res: any) => {
        if (!res.hasError) {
          this.toastr.success(res.status.message, 'Succès!');
          this.listAgent = res.items
          this.modalRef.hide()
          this.getAdmin()

        }
        else {
          this.toastr.error(res.status.message, 'Erreur!');
        }

      },
      (err: any) => {
        this.toastr.error('Echec connexion', 'Erreur!');

      },

    )
  }

  // isAdmin(item: boolean) {
  //   if (item) {
  //     return 'Admin'
  //   } else {
  //     return 'Non admin'
  //   }
  // }

  ngOnInit(): void {
    this.getAdmin()
  }
}

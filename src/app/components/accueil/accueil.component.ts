import { Component, HostListener, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { RestClientService } from 'src/app/core/services/rest-client.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';


@Component({
    selector: 'app-accueil',
    templateUrl: './accueil.component.html',
    styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
    messageError='Doit être de 13 chiffres et commencer par 384.'
    activeView = 'grid'
    index = 1;
    tableSize: number = 100;
    size = 20
    user: any
    listBenef: any=[]
    // listBenef: any = [{
    //     "photo_benef": "https://64.media.tumblr.com/0f1d9be0930e0fd6e1421e0af63b4baa/4b38c49aa49bf456-a5/s1280x1920/54be3df4f578d67626ed9b3849f53d129667b940.jpg",
    //     "matric_benef": 125455545,
    //     "prenom_benef": "KANDY OCEANA",
    //     "nom_benef": "RAND",
    //     "dt_naiss_benef": "2021-10-07T00:00:00",
    //     "bl_trt_en_cours": " ",
    //     "sexe_benef": "F",
    //     "tel_benef": " ",
    //     "fax_benef": "",
    //     "trt_en_cours_until": " ",
    //     "adr_benef": " ",
    //     "cod_statut_benef": "E",
    //     "lbc_college": "90%ME",
    //     "ASS_matric_benef": 111111111,
    //     "num_interne_pol": 3,
    //     "lbc_assur": "MCI",
    //     "prec_souscr": "MCI CARE COTE D’IVOIRE",
    //     "lbc_souscr": "3",
    //     "lb_souscr": "MCI CARE COTE D’IVOIRE"
    // },
    // {
    //     "photo_benef": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPfVCXLpCf5_CwPcuH_ut0zjdRTVpGyjFEFg&usqp=CAU",
    //     "matric_benef": 2312220,
    //     "prenom_benef": "AMELIA HONEY",
    //     "nom_benef": "RAND",
    //     "dt_naiss_benef": "2019-09-25T00:00:00",
    //     "bl_trt_en_cours": " 1234541236",
    //     "sexe_benef": "F",
    //     "tel_benef": " ",
    //     "fax_benef": "",
    //     "trt_en_cours_until": "384452254585",
    //     "adr_benef": " ",
    //     "cod_statut_benef": "E",
    //     "lbc_college": "90%ME",
    //     "ASS_matric_benef": 12548545,
    //     "num_interne_pol": 3,
    //     "lbc_assur": "MCI",
    //     "prec_souscr": "MCI CARE COTE D’IVOIRE",
    //     "lbc_souscr": "3",
    //     "lb_souscr": "MCI CARE COTE D’IVOIRE"
    // },
    // {
    //     "photo_benef": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2xW2RFl2QLAchkHyt4DDU5RA5glgOA7sjUw&usqp=CAU",
    //     "matric_benef": 1545845528,
    //     "prenom_benef": "MICHEAL ALBERTO",
    //     "nom_benef": "RAND",
    //     "dt_naiss_benef": "2016-10-19T00:00:00",
    //     "bl_trt_en_cours": " ",
    //     "sexe_benef": "M",
    //     "tel_benef": " ",
    //     "fax_benef": "",
    //     "trt_en_cours_until": " ",
    //     "adr_benef": " ",
    //     "cod_statut_benef": "E",
    //     "lbc_college": "90%ME",
    //     "ASS_matric_benef": 2545854585,
    //     "num_interne_pol": 3,
    //     "lbc_assur": "MCI",
    //     "prec_souscr": "MCI CARE COTE D’IVOIRE",
    //     "lbc_souscr": "3",
    //     "lb_souscr": "MCI CARE COTE D’IVOIRE"
    // },
    // {
    //     "photo_benef": "https://xsgames.co/randomusers/assets/avatars/male/46.jpg",
    //     "matric_benef": 11111111,
    //     "prenom_benef": "RANDOM USER",
    //     "nom_benef": "RAND",
    //     "dt_naiss_benef": "1993-01-05T00:00:00",
    //     "bl_trt_en_cours": "12345454",
    //     "sexe_benef": "M",
    //     "tel_benef": " ",
    //     "fax_benef": "0700000000",
    //     "trt_en_cours_until": "384000000000",
    //     "adr_benef": " ",
    //     "cod_statut_benef": "A",
    //     "lbc_college": "90%ME",
    //     "ASS_matric_benef": 231454155,
    //     "num_interne_pol": 3,
    //     "lbc_assur": "MCI",
    //     "prec_souscr": "MCI CARE COTE D’IVOIRE",
    //     "lbc_souscr": "3",
    //     "lb_souscr": "MCI CARE COTE D’IVOIRE"
    // }]
    listPhotos: any = [];
    modalRef: any;
    itemToSave: any = {}
    searchBeneficiaire: any
    totalItems = 0;
    busyCall: Subscription | undefined;
    listPolice: any;
    selected_police: any = {};
    excelData: any
    @ViewChild('previewTemplate', { static: false }) private previewTemplate: any;
    isFileValid: any;
    tel_benef: any;



    constructor(private authService: AuthService, private restClient: RestClientService, private modalService: BsModalService, private toastr: ToastrService, private router: Router) {
        this.user = this.authService.getUser()
        this.tel_benef = this.authService.getLoginInfo().data.tel_benef
        console.log('user: ', this.user);
        console.log('logininfo: ',  this.authService.getLoginInfo().data.tel_benef);
        console.log('user.id: ', this.user.id);
        console.log('user.polices: ', this.user.polices);

    }

    readExcelFile(event:any) {
        this.excelData=[]
        let file = event.target.files[0]
        let fileReader = new FileReader()
        fileReader.readAsBinaryString(file)
        fileReader.onload = (event) => {
            let data = fileReader.result
            let workbook = XLSX.read(data, {type:'binary'})
            let sheetNames = workbook.SheetNames
            this.excelData = XLSX.utils.sheet_to_json(workbook.Sheets[sheetNames[0]])

            this.excelData.map((ed:any)=>{
                let cmu = ed.trt_en_cours_until
                cmu = cmu+''
                if(cmu && cmu.length){
 
                    ed.isValid =cmu.lastIndexOf('384', 0) === 0
                }
            })
            this.isFileValid = !!this.excelData.filter((ed:any)=>!ed.isValid).length
            this.openModalPreview()
        }     
    }



    getBeneficiaires() {
        let request = {
            index: this.index,
            size: this.size,
            user: this.user.id || this.user.matric_benef,
            assureur: this.user.assureur || null,
            data: {

                matric_benef: this.user.matric_benef || this.searchBeneficiaire,
                lbc_assur: this.user.lbc_assur,
                num_interne_police: this.selected_police?.num_interne_pol
            }
        }

        this.busyCall = this.restClient.execute('benefCmu/getListeFamille', request).subscribe(
            (res: any) => {
                if (!res.hasError) {
                    this.listBenef = res.items
                    console.log('list benef: ', this.listBenef);
                    this.listBenef.map((lb:any)=>{
                        lb.isEditable1=false
                        lb.isEditable2=false
                        lb.isEditable3=false
                        // lb.hasPhoneNumber = !!(lb.fax_benef && lb.fax_benef.length)
                        lb.hasPhoneNumber = false
                        lb.trt_en_cours_until = lb.trt_en_cours_until.trim()

                    })

                    // Define the custom order
                    const order = ['A', 'C', 'E'];

                    // Sort items based on cod_statut_benef
                    this.listBenef.sort((a:any, b:any) => {
                        const indexA = order.indexOf(a.cod_statut_benef);
                        const indexB = order.indexOf(b.cod_statut_benef);
                        
                        if (indexA === -1 && indexB === -1) {
                            return 0; // both are at the bottom
                        } else if (indexA === -1) {
                            return 1; // a is at the bottom
                        } else if (indexB === -1) {
                            return -1; // b is at the bottom
                        } else {
                            return indexA - indexB; // sort by custom order
                        }
                    });

                    console.log('after sort: ',this.listBenef)

                    this.totalItems = res.count
                }
                else {
                    alert(res.status.message)
                }

            },
            (err: any) => {
                alert('erreur de connexion')
            },

        )
    }


    sendSms(benef:any) {
        let request = {
            index: this.index,
            size: this.size,
            user: this.user.id || this.user.matric_benef,
            assureur: this.user.assureur || null,
            data:{
                id_pays:"+225",
                destinataire: this.tel_benef,
                message: 'Félicitations ! Vous venez de modifier les informations de '+benef.nom_benef+' '+benef.prenom_benef+' avec succès'
            }
        }

        this.busyCall = this.restClient.execute('customQuery/sendSms', request).subscribe(
            (res: any) => {
                if (!res.hasError) {

                }
                else {
                    alert(res.status.message)
                }

            },
            (err: any) => {
                alert('erreur de connexion')
            },

        )
    }


    openModalPreview(){
        let config = { backdrop: true, ignoreBackdropClick: true, class: 'modal-custom' };
        this.modalRef = this.modalService.show(this.previewTemplate, config);
    }

    openModal(template: TemplateRef<any>, data: any) {
        let config = { backdrop: true, ignoreBackdropClick: true, class: 'modal-custom' };

        if (data) {
            this.itemToSave = { ...data }
            this.itemToSave.dt_naiss_benef = moment(this.itemToSave.dt_naiss_benef).format('DD/MM/YYYY')
        }
       
        this.modalRef = this.modalService.show(template, config);

    }

    confirmUpload() {
        // des control sur la liste à importer ici
       
        Swal.fire({
            title:'Attention',
            text:'Voulez vous importez ces données ?',
            icon:'warning',
            showCancelButton: true,
            confirmButtonText:'Oui',
            confirmButtonColor: '#426DA9',
            cancelButtonColor: 'red',
            cancelButtonText:'Non'
          }).then((result) => {
            if (result.value){
              this.saveItem(this.excelData,true)
            } else {
              this.router.navigate(['/accueil'])
            }
          })
    }

    // verificationCmu(cmu:any, data?:any){
    //     let numero_cmu = cmu
    //     let pos1 = numero_cmu.charAt(0)
    //     let pos2 = numero_cmu.charAt(1)
    //     let pos3 = numero_cmu.charAt(2)

    //     if (pos1 != '3'){
    //         this.toastr.error('Le numéro CMU doit commencer par 3 !', 'Erreur!');
    //         return
    //     }
    //     if (pos2 != '8'){
    //         this.toastr.error('Le numéro CMU doit commencer par 38 !', 'Erreur!');
    //         return
    //     }
    //     if (pos3 != '4'){
    //         this.toastr.error('Le numéro CMU doit commencer par 384 !', 'Erreur!');
    //         return
    //     }

    //     if (numero_cmu.length === 13){
    //         Swal.fire({
    //             title:'Attention',
    //             text:'Voulez vous sauvegardez ces données ?',
    //             icon:'warning',
    //             showCancelButton: true,
    //             confirmButtonText:'Oui',
    //             confirmButtonColor: '#426DA9',
    //             cancelButtonColor: 'red',
    //             cancelButtonText:'Non'
    //             }).then((result) => {
    //             if (result.value){
    //                 this.saveItem(data)
    //             } else {
    //                 this.router.navigate(['/accueil'])
    //             }
    //         })

    //     } else {
    //         this.toastr.error('Le numéro CMU doit comporter 13 caracteres !', 'Erreur!');
    //     }

    // }

    confirmSave(data: any,key?:any) {
        console.log('data: ', data);;
        console.log('trt_en_cours_until: ',data.trt_en_cours_until);
        console.log('tel_benef: ',data.tel_benef);
        console.log('trt_en_cours_until: ',data.trt_en_cours_until);
        console.log('key: ',key)


        if(key=='isEditable1' && !data.trt_en_cours_until){
            this.toastr.error('Veuillez renseigner le numéro cmu svp !', 'Erreur!');
            return
        }
        else{
            let numero_cmu = data.trt_en_cours_until
            let pos1 = numero_cmu.charAt(0)
            let pos2 = numero_cmu.charAt(1)
            let pos3 = numero_cmu.charAt(2)

            if (pos1+pos2+pos3 != '384'){
                this.toastr.error('Le numéro CMU doit commencer par 384 !', 'Erreur!');
                return
            }

            if (numero_cmu.length != '13'){
                this.toastr.error('Le numéro CMU doit etre de 13 chiffres !', 'Erreur!');
                return
            }
            const isNumeric = /^[0-9]+$/.test(numero_cmu);

            if (!isNumeric){
                this.toastr.error('Le numéro CMU doit comporter des chiffres uniquement !', 'Erreur!');
                return
            }

        }
        if(key=='isEditable2' && !data.bl_trt_en_cours){
            this.toastr.error('Veuillez renseigner le numéro cnps svp !', 'Erreur!');
            return
        }
        if(key=='isEditable3' && !data.fax_benef){
            this.toastr.error('Veuillez renseigner le numéro de téléphone svp !', 'Erreur!');
            return
        }
    
        

            Swal.fire({
                title:'Attention',
                text:'Voulez vous sauvegardez ces données ?',
                icon:'warning',
                showCancelButton: true,
                confirmButtonText:'Oui',
                confirmButtonColor: '#426DA9',
                cancelButtonColor: 'red',
                cancelButtonText:'Non'
                }).then((result) => {
                if (result.value){
                    this.saveItem(data,key)
                } else {
                    // this.router.navigate(['/accueil'])
                    if(key)
                        this.onToggleEdit(data,0,key)
                }
            })


        // let result = confirm("Voulez vous sauvegarder ces données?");
        // if (result == true) {
        //     this.saveItem(data)
        // } else {

        // }


    }


    confirmSave_bis(data: any) {
     

        if (!data || !data.trt_en_cours_until) {
            this.toastr.error('Veuillez renseigner numéro de securité social !', 'Erreur!');
            return
        }

        console.log('data.trt_en_cours_until: ',data.trt_en_cours_until)
        console.log('data.trt_en_cours_until.length: ',data.trt_en_cours_until.length)

        const isNumeric = /^[0-9]+$/.test(data.trt_en_cours_until);
        if (!isNumeric) {
            this.toastr.error('Veuillez renseigner que des entiers !', 'Erreur!');
            return
        }

        if (data.trt_en_cours_until.length !=13) {
            this.toastr.error('Le numéro cmu doit comporter 13 caractères !', 'Erreur!');
            return
        }
        let isValidCmu=this.startsWith(data.trt_en_cours_until,'384')
        if (!isValidCmu) {
            this.toastr.error('Le numéro cmu doit commencer par 384 !', 'Erreur!');
            return
        }
        // if (!data.fax_benef) {
        //     this.toastr.error('Veuillez renseigner numéro téléphone !', 'Erreur!');
        //     return
        // }

        console.log('fax_benef: ',data.fax_benef)



        Swal.fire({
            title:'Attention',
            text:'Voulez vous sauvegardez ces données ?',
            icon:'warning',
            showCancelButton: true,
            confirmButtonText:'Oui',
            confirmButtonColor: '#426DA9',
            cancelButtonColor: 'red',
            cancelButtonText:'Non'
            }).then((result) => {
            if (result.value){
                this.saveItem(data,'')
            }
        })


    }


startsWith(str:string, word:string) {
    return str.lastIndexOf(word, 0) === 0;
}

    saveItem(item: any,key:any,isUplaod?:Boolean) {
        console.log('isUplaod',isUplaod);
        
        // let body = isUplaod?item:
        // [{
        //     matric_benef:item.matric_benef,
        //     trt_en_cours_until: item.trt_en_cours_until,
        //     bl_trt_en_cours: item.bl_trt_en_cours,
        //     tel_benef: item.tel_benef
        // }]


        let request = {
            user: this.user.id || this.user.matric_benef,
            datas: [{
                matric_benef:item.matric_benef,
                trt_en_cours_until: item.trt_en_cours_until,
                bl_trt_en_cours: item.bl_trt_en_cours,
                // tel_benef: item.tel_benef
                tel_benef: item.fax_benef
            }]
        }

        this.busyCall = this.restClient.execute('benefCmu/benefUpdate', request).subscribe(
            (res: any) => {
                if (!res.hasError) {
                    // this.toastr.success(res.status.message, 'Succès!');
                    
                    this.sendSms(item)
                    this.listBenef = res.items

                    

                    if(this.modalRef)
                        this.modalRef.hide()
                    if(key)
                        this.onToggleEdit(item,0,key)
                    this.getBeneficiaires()
                    Swal.fire(
                        'Vous avez terminé !',
                        'Félicitations ! MCI CARE CI vous remercie pour votre fidélité.',
                        'success'
                      )

                }
                else {
                    this.toastr.error(res.status.message, 'Erreur!');
                }

            },
            (err: any) => {
                this.toastr.error('Echec connexion', 'Erreur!');

            },

        )
    }

    pageChanged(event: any) {
        this.index = event.page;
        this.getBeneficiaires();
    }

    getPoliceByGarant(currentSelectedGarant?: any) {
        console.log('this.itemToSave.lbc_assur: ', this.itemToSave.lbc_assur);

        let request = {
            index: this.index,
            size: this.size,
            user: this.user.id,
            data: {
                // lbc_assur: currentSelectedGarant?.lbc_assur || this.user?.assureur,
                isAdmin: this.user?.isAdmin
            }
        }

        console.log('test')

        // this.busyCall = this.restClient.execute('police/getByCriteria', request).subscribe(
        this.busyCall = this.restClient.execute('police/getActivePolice', request).subscribe(

            (res: any) => {
                if (!res.hasError) {


                    this.listPolice = res.items
                    console.log('list police: ', this.listPolice);

                }
                else {
                    this.toastr.error(res.status.message, 'Erreur!');
                }

            },
            (err: any) => {
                this.toastr.error('Echec connexion', 'Erreur!');
            },

        )
    }

    onToggleEdit(it:any,val:any,field:any){
        it[field] = !!val
    }

    zoomInImage(item:any){
        item.isBiggerImg = !item.isBiggerImg
        // console.log('item: ',item);
        
    }

    zoomOut(){
        if(this.listBenef && this.listBenef.length)
            this.listBenef.map((lb:any)=>lb.isBiggerImg=false)
    }

    // @HostListener('document:click', ['$event'])
    // onClick(event: MouseEvent) {
    //     // this.zoomOut()
    // }
    validateNumeroCmu(): boolean {
        // console.log('jkfjdjkj')
        const value = this.itemToSave.trt_en_cours_until;
        const isNumeric = /^[0-9]+$/.test(value);
        let checkStart = value.startsWith('384')
        let val = isNumeric && value.length === 13 && checkStart;
        return val;
      }
    validateNumeroCmuQuick(item:any): boolean {
        // console.log('jkfjdjkj')
        const value = item.trt_en_cours_until;
        const isNumeric = /^[0-9]+$/.test(value);
        let checkStart = value.startsWith('384')
        let val = isNumeric && value.length === 13 && checkStart;
        return val;
      }
    ngOnInit(): void {
        this.getBeneficiaires()
    }
}

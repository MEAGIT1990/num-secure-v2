import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/services/auth.service';
import { RestClientService } from 'src/app/core/services/rest-client.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-auth-admin',
  templateUrl: './auth-admin.component.html',
  styleUrls: ['./auth-admin.component.css']
})
export class AuthAdminComponent implements OnInit {

  public aFormGroup!: FormGroup;
    user: any = {}
    isVerify = false
    assure: any = {}
    isLoading = false
    isLoadingAdmin=false
    siteKey: string = "6LcRp2UeAAAAAFg0nxi_XukyzU7a7aJKox3riopQ"
    // siteKey: string = "6LfWZpAgAAAAAOvjfS5we-MN23x4o6PXmGy_a4WC"

    constructor(private authService: AuthService, private formBuilder: FormBuilder, private toastr: ToastrService) { }

    ngOnInit() {
        // this.aFormGroup = this.formBuilder.group({
        //     recaptcha: ['', Validators.required]
        // });
    }

    onSubmitAdmin() {
        if (!this.user.login) {
            this.toastr.error('Veuillez renseigner login svp !', 'Erreur!');
            return
        }
        if (!this.user.password) {
            this.toastr.error('Veuillez renseigner mot de passe svp !', 'Erreur!');
            return
        }
        this.isLoadingAdmin = true
        this.authService.loginAdminAuth(this.user).then(as=>this.isLoadingAdmin=false)
    }

    onVerify(event: any) {
        this.isVerify = true
        console.log('is verify ', this.isVerify);
    }


}

import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { reduce, Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { RestClientService } from 'src/app/core/services/rest-client.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-otp-modal',
    templateUrl: './otp-modal.component.html',
    styleUrls: ['./otp-modal.component.css']
})
export class OtpModalComponent implements OnInit {
    busyCall: Subscription | undefined;
    itemToSave: any = {};
    currentData: any
    currentUser: any;
    busyGet: Subscription | undefined
    modalRef: any;
    isLoading = false
    user : any = {};
    remainingTime: number = 0;
    minutes: number=3;
    seconds: number=0;
    interval: any;


    constructor(private toastr:ToastrService, private router:Router,public bsModalRef: BsModalRef, private authService: AuthService, private restClient: RestClientService, private modalService: BsModalService) {
        setTimeout(() => {
            if (this.currentData) {
                this.itemToSave = Object.assign({}, this.currentData);
                console.log('itemToSave', this.itemToSave);
                this.itemToSave = this.itemToSave
            }
        }, 400);

    }

    confirmSave(data: any) {
        this.isLoading=true
        if (!data.otp) {
            Swal.fire(
                'Erreur code de verification !',
                'Assurez vous que vous avez saisi le bon code de verification !',
                'error'
            )
            // alert('Veuillez renseigner otp')
            return
        }

            let request = {
                data: {
                    matric_benef: data.matric_benef,
                    otp: data.otp
                }
            }
            
            this.busyCall = this.restClient.execute('benefCmu/confirmOtp', request).subscribe(
                    (res: any) => {
                        this.isLoading=false

                        if (!res.hasError) {
                                this.bsModalRef.hide()
                                localStorage.setItem("currentUser", JSON.stringify(res.items[0]));
                                this.router.navigate(['/accueil'])
    
                            }
                        else {
                            this.toastr.error(res.status.message, 'Erreur!');
                        }
                    },
                    (error: any) => {
                        this.isLoading=false

                        this.toastr.error('Erreur de communication', 'Erreur!');
                    }
                )
    }

    resendOtp(assure:any) {
        let request = {
            data: {
                matric_benef: assure.matric_benef,
                tel_benef: assure.tel_benef,
                dt_naiss_benef: assure.dt_naiss_benef,
                otp: assure.otp
            }
        }
    
        this.busyCall = this.restClient.execute('benefCmu/auth', request).subscribe(
            (res: any) => {
                if (!res.hasError) {
                    
                    this.stopTimer()
                    this.minutes = 3
                    this.seconds = 0
                    this.startTimer();
                // this.toastr.success(res.status.message);
                this.toastr.success("Code envoyé avec succès. Veuillez consulter vos sms");

                }
                else {
                    this.toastr.error(res.status.message, 'Erreur!');

                }
            },
            (error: any) => {
                this.toastr.error('Erreur de communication', 'Erreur!');

            }
        )
    }


    onOtpChange(event:any){
        console.log('event: ',event);
        this.itemToSave.otp = event
        
      }

      startTimer() {
        this.interval = setInterval(() => {
          if (this.seconds > 0) {
            this.seconds--;
          } else {
            if (this.minutes === 0) {
              // Countdown is finished
              this.stopTimer();
              return;
            }
            this.minutes--;
            this.seconds = 59;
          }
        }, 1000);
      }

      stopTimer() {
        clearInterval(this.interval);
      }

    ngOnInit(): void {
    this.startTimer();


    }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/services/auth.service';
import { RestClientService } from 'src/app/core/services/rest-client.service';
import Swal from 'sweetalert2';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
    selector: 'app-connexion',
    templateUrl: './connexion.component.html',
    styleUrls: ['./connexion.component.css'],
    animations: [
        trigger('fadeIn', [
          transition(':enter', [
            style({ opacity: 0 }),
            animate('1000ms', style({ opacity: 1 })),
          ]),
        ]),
      ],
})
export class ConnexionComponent implements OnInit {
    showTooltip=true
    public aFormGroup!: FormGroup;
    animationState: string = 'fadeIn';
    user: any = {}
    isVerify = false
    assure: any = {}
    isLoading = false
    isLoadingAdmin=false
    isShowPop=true
    siteKey: string = "6LcRp2UeAAAAAFg0nxi_XukyzU7a7aJKox3riopQ"
    maxDate = new Date(new Date().setFullYear(new Date().getFullYear() - 1))
    // maxDate = new Date()
    // siteKey: string = "6LfWZpAgAAAAAOvjfS5we-MN23x4o6PXmGy_a4WC"

    constructor(private authService: AuthService, private formBuilder: FormBuilder, private toastr: ToastrService) {
        this.assure.dt_naiss_benef = new Date();
     }

    ngOnInit() {
        // this.aFormGroup = this.formBuilder.group({
        //     recaptcha: ['', Validators.required]
        // });
        localStorage.removeItem("currentUser");
    }

    onSubmitGarant() {
        if (!this.user.login) {
            this.toastr.error('Veuillez renseigner login svp !', 'Erreur!');
            return
        }
        if (!this.user.password) {
            this.toastr.error('Veuillez renseigner mot de passe svp !', 'Erreur!');
            return
        }
        this.isLoadingAdmin = true
        this.authService.login(this.user).then(as=>this.isLoadingAdmin=false)
    }

    onVerify(event: any) {
        this.isVerify = true
        console.log('is verify ', this.isVerify);
    }

    onSubmitAssure() {
        if (!this.assure.matric_benef) {
            this.toastr.error('Veuillez renseigner votre matricule svp!', 'Erreur!');
            return
        } else if (!this.assure.dt_naiss_benef) {
            this.toastr.error('Veuillez renseigner votre date de naissance svp!', 'Erreur!');
            return

        } else if (!this.assure.tel_benef) {

            this.toastr.error('Veuillez renseigner votre contact svp!', 'Erreur!');
            return
        }

        // console.log('isVerify : ', this.isVerify);
        // if (!this.isVerify) {
        //     alert('Renseigné le captcha !')
        //     this.toastr.error('Veuillez valider le captcha !', 'Erreur!');
        //     return
        // }
        this.isLoading = true
        this.authService.loginAssure(this.assure).then(la=>{
            this.isLoading = false
        })
    }

}

import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-main-layout',
    templateUrl: './main-layout.component.html',
    styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent implements OnInit {
    user: any = {}
    constructor(private authService: AuthService, public router: Router) {
        this.user = authService.getUser()
    }

    ngOnInit(): void {
    }

    deconnexion() {
        this.authService.logout()
    }

    administration() {
        // this.router.navigateByUrl('admin')
        this.authService.admin()
    }

    onImage() {
        this.authService.accueil()

    }

    getUserRole(): any {
        if (this.user.isadmin && this.user.isdsi) {
            return '(Admin DSI)'
        }
        if (this.user.isadmin && !this.user.isdsi) {
            return '(Admin)'
        }
        if (!this.user.isadmin && !this.user.isdsi) {
            return '(Assuré)'
        }
    }

}

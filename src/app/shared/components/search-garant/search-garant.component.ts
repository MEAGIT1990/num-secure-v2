import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { RestClientService } from 'src/app/core/services/rest-client.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-search-garant',
  templateUrl: './search-garant.component.html',
  styleUrls: ['./search-garant.component.scss']
})
export class SearchGarantComponent implements OnInit {

  @Output() itemGarantSelected = new EventEmitter<any>();
  @Input() cancelItemSelected: any;
  @Input() isClearUp: any;

  modalRef: any;
  itemToSearch: any = {};
  itemToSave: any = {};
  listItem: Array<any> = [];
  user: any = {};
  busyGet: any;
  itemSelected: any = {};
  canDisplay: boolean = false;
  isForCompta: boolean = true

  constructor(private authService: AuthService, private restClient: RestClientService, private modalService: BsModalService, private toastr: ToastrService) {
    this.user = this.authService.getUser()
  }


  getItems() {

    // On affiche le tableau des patients
    this.canDisplay = true;

    let request = {
      user: this.user.id,
      data: {
        lb_assur: this.itemToSearch.searchText ? this.itemToSearch.searchText : null
      },
      index: 1,
      size: 10
    }
    console.log('data sent: ', JSON.stringify(request));

    this.busyGet = this.restClient.execute('assureurAssureur/getByCriteria', request)
      .subscribe(
        (res: any) => {
          if (res && res['items']) {
            this.listItem = res['items'];
          }
          else {
            this.listItem = [];
          }
        },
        (err: any) => {
        }
      );
  }


  openModal(data: any, template: TemplateRef<any>) {

    let config = { backdrop: true, ignoreBackdropClick: true };

    this.itemToSave = {};
    if (data) {
      // Lorsque nous sommes en modification
      this.itemToSave = Object.assign({}, data);
    }

    this.modalRef = this.modalService.show(template, Object.assign({}, config, { class: 'modal-lg modal-width-75' }));
  }

  selectedItem(item: any) {
    console.log('item', item);

    if (item) {
      this.itemSelected = { ...item };
      this.itemToSearch.searchText = this.itemSelected.lb_assur;

      // Brodcast de la valeur
      this.itemGarantSelected.emit(this.itemSelected);

      // On ferme le tableau de selection des patients
      this.canDisplay = false;
      this.itemToSearch.searchText = item.lb_assur
      if (this.isClearUp == 'true') {
        this.itemToSearch.searchText = null
      }


    }
  }

  patientSaved($event: any) {
    if ($event) {
      this.selectedItem($event);

      // Après ça on ferme le modal
      this.modalRef.hide();
    }
  }

  ngOnInit(): void {

  }


  ngOnChanges(changes: SimpleChanges) {

    if (changes['cancelItemSelected'] && changes['cancelItemSelected'].currentValue) {
      this.itemSelected = {};
      this.itemToSearch.searchText = null;
    }
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchGarantComponent } from './search-garant.component';

describe('SearchGarantComponent', () => {
  let component: SearchGarantComponent;
  let fixture: ComponentFixture<SearchGarantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchGarantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchGarantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

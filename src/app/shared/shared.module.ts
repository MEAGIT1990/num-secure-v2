import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchGarantComponent } from './components/search-garant/search-garant.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgBusyModule } from 'ng-busy';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NgxMaskModule } from 'ngx-mask';
import { ToastrModule } from 'ngx-toastr';
import { NgOtpInputModule } from 'ng-otp-input';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SafePipe } from '../core/pipes/safe.pipe';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TruncatePipe } from '../core/pipes/truncate.pipe';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [
    SearchGarantComponent,
    SafePipe,
    TruncatePipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    PaginationModule.forRoot(),
    NgBusyModule,
    NgxMaskModule.forRoot(),
    NgxCaptchaModule,
    ReactiveFormsModule,
    NgOtpInputModule,
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    NgxSpinnerModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule,
    PaginationModule,
    NgBusyModule,
    NgxMaskModule,
    NgxCaptchaModule,
    ReactiveFormsModule,
    SearchGarantComponent,
    NgOtpInputModule,
    BsDatepickerModule,
    SafePipe,
    TooltipModule,
    TruncatePipe,
    NgxSpinnerModule

  ]
})
export class SharedModule { }
